# mandatory vars:

binary_name			:= test_db
type				:= exec

# optional vars:
default_targets			:= format debug run

include_paths			:= lib/pq/install/include lib/sqlpp11/install/include
library_paths			:= lib/pq/install/lib lib/sqlpp11/install/lib

#source_extension		?= .cpp
source_paths			:= src src/tdb
libs_to_link			:= pq

main_source_paths		:= src/main

#test_source_paths		?=
#test_add_libs_to_link		?=

#output_folder			?= build
#DESTDIR				?=
PREFIX				:= $(CURDIR)/install

format_dirs			:= src
#format_extensions		?= .hpp .cpp

#subcomponent_paths		?=

include lib/generic_makefile/modular_makefile.mk
